#ifdef assetManager_config_assetType_hpp
#error Multiple inclusion
#endif
#define assetManager_config_assetType_hpp

#ifndef common_hpp
#error "Please include common.hpp before this file"
#endif

namespace assetManager
{
  namespace config
  {
    namespace assetType
    {
      enum Enum
      {
        RAW_FILE,
        MAGICAVOXEL_MODEL,
        SHADER_PROGRAM,
        ENUM_COUNT
      };

      Str NAME[];

      DECLARE_ENUM_PREINCREMENT
    }
  }
}
