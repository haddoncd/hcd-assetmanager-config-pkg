#ifdef assetManager_config_assetType_cpp
#error Multiple inclusion
#endif
#define assetManager_config_assetType_cpp

#ifndef assetManager_config_assetType_hpp
#include "assetManager_config_assetType.hpp"
#endif

namespace assetManager
{
  namespace config
  {
    namespace assetType
    {
      Str NAME[ENUM_COUNT] =
      {
        "raw_file"_s,
        "magicavoxel_model"_s,
        "shader_program"_s
      };

      DEFINE_ENUM_PREINCREMENT
    }
  }
}
