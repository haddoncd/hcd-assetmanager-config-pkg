#ifdef assetManager_config_hpp
#error Multiple inclusion
#endif
#define assetManager_config_hpp

#ifndef common_hpp
#error "Please include common.hpp before this file"
#endif

#ifndef Str_hpp
#error "Please include Str.hpp before this file"
#endif

#ifndef StrBuf_hpp
#error "Please include StrBuf.hpp before this file"
#endif

#ifndef print_hpp
#error "Please include print.hpp before this file"
#endif

#ifndef assetManager_config_assetType_hpp
#error "Please include assetManager_config_assetType.hpp before this file"
#endif

namespace assetManager
{
  namespace config
  {
    struct Entry
    {
      assetType::Enum type;
      Str             name;
      Str             params;
    };

    template <typename S>
    bool parse(Str *text, Entry *entry, S err)
    {
      bool found_entry = false;

      while (!found_entry && text->length)
      {
        Str type_str = Str::empty();

        Str line = text->chop({ "\r\n"_s, "\n"_s });

        Str remaining_line = line.truncatedAtFirst('#');

        while (remaining_line.length && !type_str.length)
        {
          type_str = remaining_line.chop(' ');
        }

        if (!type_str.length) continue;

        entry->name = Str::empty();
        while (remaining_line.length && !entry->name.length)
        {
          entry->name = remaining_line.chop(' ');
        }

        entry->params = remaining_line.removeLeading(' ');

        if (!entry->name.length || !entry->params.length)
        {
          println(err,
            "Ignoring invalid asset: \""_s, line, "\""_s
          );
          continue;
        }

        // TODO: use a hash table for this?
        for (assetType::Enum e = (assetType::Enum)0;
             e < assetType::ENUM_COUNT; ++e)
        {
          if (type_str == assetType::NAME[e])
          {
            entry->type = e;
            found_entry = true;
            break;
          }
        }

        if (!found_entry)
        {
          println(err,
            "Ignoring asset with unknown type: \""_s, line, "\""_s
          );
          continue;
        }
      }

      return found_entry;
    }
  }
}
